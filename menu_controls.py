#!/usr/bin/python
from tkinter import *
import subprocess
import time
from functools import partial

name_and_commands = [
    ("US_kb", "setxkbmap -layout us"),
    ("BR_kb", "setxkbmap -layout us -variant intl -option nodeadkeys"),
    ("transmission", "transmission-gtk"),
    ("signal", "signal-desktop"),
    ("gimp", "gimp"),
    ("calibre", "calibre"),
    ("mirage", "mirage"),
    ("vlc", "vlc"),
    ("pavucontrol", "pavucontrol"),
    ("arandr", "arandr"),
    ("steam", "steam"),
    ("lutris", "lutris"),
    ("screenshot", f"flameshot gui"),
    ("bluetooth connect", '''echo "connect CC:98:8B:64:02:BB" | bluetoothctl'''),
    ("bluetooth disconnect", '''echo "disconnect" | bluetoothctl'''),
    ("discord", "discord"),
    ("android-file-transfer", "android-file-transfer"),
    ("slack", "slack"),
    ("telegram", "telegram-desktop"),
    ("firefox", "firefox"),
    ("gba_emulator", "mgba-qt"),
    ("snes_emulator", "snes9x-gtk"),
    ("map", "qgis"),
    ("anki", "anki"),
]


def execute_command(command):
    process = subprocess.Popen(command, shell=True,)


class App:
    def __init__(self, master):
        self.frame = Frame(master)
        self.frame.pack()
        self.frame.bind('<Escape>', self.quit)

        # Setup buttons
        for name, exec_command in name_and_commands:
          self.button = Button(
              self.frame, text=name, command=partial(
                  self.exec_and_quit, exec_command))
          self.button.pack(side=BOTTOM)

        # Setup search box
        self.search_string = StringVar()
        self.search_box = Entry(
            self.frame,
            width=38,
            textvariable=self.search_string)
        self.search_box.pack(side=TOP)
        self.search_box.bind('<Enter>', self.execute_top_result)
        self.search_box.focus_set()
        self.search_string.trace_add("write", self.update_command_list)

        # Setup result list
        self.results = Text(self.frame, width=40, height=5)
        self.results.pack(side=TOP)

    def update_command_list(self, *args):
        self.results.delete('1.0', 'end')
        text = self.search_string.get()
        space_used = text and text[-1] == ' '
        if space_used:
          text = text[:-1]

        results = [name for ( name, _) in name_and_commands if name.lower().find(text) != - 1]
        cmds = [cmd for ( name, cmd) in name_and_commands if name.lower().find(text) != - 1]
        if results and space_used:
          self.exec_and_quit(cmds[0])

        self.results.insert('end', "\n".join(results))


    def exec_and_quit(self, cmd):
        execute_command(cmd)
        self.frame.quit()

    def execute_top_result(self, event):
        text = self.search_string.get()
        if len(text) == 0:
          return
        cmds = [cmd for ( name, cmd) in name_and_commands if name.lower().find(text) != - 1]
        if len(cmds) == 0:
          return
        self.exec_and_quit(cmds[0])

    def quit(self, event):
        self.frame.quit()


root = Tk()
#Forces i3 to use a floating window:
root.attributes('-type', 'dialog')

app = App(root)
root.mainloop()
root.destroy()
